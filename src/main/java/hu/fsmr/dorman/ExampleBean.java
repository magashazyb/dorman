package hu.fsmr.dorman;

import hu.fsmr.dorman.auth.model.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean(name = "exampleBean")
@SessionScoped
public class ExampleBean {

    public static void main(String[] args) {
        String[][][][] a = new String[0][0][0][0];
        System.out.println("a");
    }
    private List<ExampleClass> exampleVar;

    @ManagedProperty(value = "#{authBean.auth}")
    private User user;

    public ExampleBean(){
        refresh();
    }

    public void refresh(){
        exampleVar = ExampleClass.queryAll();
    }

    public String getUserName(){ return user.getUsername(); }

    public List<ExampleClass> getExampleVar() {
        return exampleVar;
    }

    public void setExampleVar(List<ExampleClass> exampleVar) {
        this.exampleVar = exampleVar;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}









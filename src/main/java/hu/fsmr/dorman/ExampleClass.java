package hu.fsmr.dorman;

import hu.fsmr.dorman.configuration.HibernateConfiguration;
import org.hibernate.Criteria;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "example_table")
public class ExampleClass {
    @Id
    @Column(name="pk")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long examplePk;

    public static List<ExampleClass> queryAll(){
        List<ExampleClass> examples = new ArrayList<ExampleClass>();
        //...
        Session session = HibernateConfiguration.getSession();
        Criteria criteria = session.createCriteria(ExampleClass.class);
        examples = criteria.list();
        //...
        return examples;
    }

    public long getExamplePk() {
        return examplePk;
    }

    public void setExamplePk(long examplePk) {
        this.examplePk = examplePk;
    }
}




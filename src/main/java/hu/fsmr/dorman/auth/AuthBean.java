package hu.fsmr.dorman.auth;


import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.utils.HttpUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * Created by blase on 2017. 01. 25..
 */
@ManagedBean(name = "authBean")
@SessionScoped
public class AuthBean implements Serializable{

    private String password;
    private String userName;
    private boolean loginState = false;

    private User auth;

    public String login() {
        if ((auth = User.login(userName, password)) != null) {
            System.out.println("logged in: " + auth.getUsername());
            System.out.println("with role: " + auth.getRole().getRoleName());
            HttpSession session = HttpUtil.getSession();
            session.setAttribute("username", auth.getUsername());
            return "home";
        } else {
            loginState = true;
            return "logout";
        }
    }

    public String logout() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.invalidateSession();
       try {
            ec.redirect("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "home";
    }

    public void test() {
        System.out.println("test");
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getAuth() {
        return auth;
    }

    public void setAuth(User auth) {
        this.auth = auth;
    }

    public boolean isLoginState() {
        return loginState;
    }

    public void setLoginState(boolean loginState) {
        this.loginState = loginState;
    }
}

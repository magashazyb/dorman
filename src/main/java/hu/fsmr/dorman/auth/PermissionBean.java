package hu.fsmr.dorman.auth;

import hu.fsmr.dorman.auth.model.Role;
import hu.fsmr.dorman.user.UserBean;
import org.hibernate.exception.ConstraintViolationException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by blase on 2017.10.08..
 */
@ManagedBean(name= "permissionBean")
@SessionScoped
public class PermissionBean implements Serializable{

    private List<Role> rolesList;

    private Role selectedRole;

    @ManagedProperty(value="#{userBean}")
    private UserBean userBean;

    public PermissionBean(){
        refresh();
    }

    private void refresh(){
        rolesList = Role.queryRolesList();
    }


    public boolean isGranted(String role){
        if( role.toLowerCase().contains(userBean.getUser().getRole().getRoleName().toLowerCase())){
            return  true;
        }
        return false;
    }

    public void newRole() {
        selectedRole = new Role();
    }

    public void save(){
        try{
            selectedRole.save();
            refresh();
        }catch (ConstraintViolationException e){
            FacesMessage message = new FacesMessage("Succesful is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void delete() throws Exception{
        try{
            selectedRole.delete();
            refresh();
        }catch (ConstraintViolationException e){
            e.printStackTrace();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Role> getRolesList() {
        refresh();
        return rolesList;
    }

    public void setRolesList(List<Role> rolesList) {
        this.rolesList = rolesList;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Role getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }
}

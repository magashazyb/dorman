package hu.fsmr.dorman.auth;

import hu.fsmr.dorman.auth.model.Role;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
/**
 * Created by blase on 11/19/16.
 */
@FacesConverter(value = "roleConverter")
public class RoleConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return Role.getRoleById(new Long(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        Long id = ((Role)o).getRoleId();
        return String.valueOf(id);
    }
}

package hu.fsmr.dorman.auth.model;

import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blase on 2016. 10. 16..
 */

@Entity
@Table(name="roles")
public class Role{

    @Id
    @Column(name="role_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long roleId;

    @Column(name="role_name")
    private String roleName;

    public Role(){
        roleName=new String();
    }

    public static List<Role> queryRolesList(){
        List<Role> roles = new ArrayList<Role>();
        Session session = null;
        try{
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(Role.class);
            roles = criteria.list();
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            if( session != null ){
                session.close();
            }
        }
        return roles;
    }

    public static Role getRoleById(Long RolePk){
        Session session = null;
        Role role = null;
        try{
            session = HibernateConfiguration.getSession();
            role = (Role) session.get(Role.class, RolePk);
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return role;
    }

    public static Role getRoleByName(String roleName){
        Session session = null;
        Role role = null;
        try{
            session = HibernateConfiguration.getSession();
            Criteria criteria = session.createCriteria(Role.class)
                    .add(Restrictions.eq("roleName", roleName));
            role = (Role) criteria.uniqueResult();
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            session.close();
        }
        return role;
    }

    public void save() throws Exception{
        HibernateCommon.save(this);
    }

    public void delete() throws Exception{
        HibernateCommon.delete(this);
    }



    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (roleId != role.roleId) return false;
        return roleName.equals(role.roleName);
    }

    @Override
    public int hashCode() {
        int result = (int) (roleId ^ (roleId >>> 32));
        result = 31 * result + roleName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}

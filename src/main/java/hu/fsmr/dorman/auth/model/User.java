package hu.fsmr.dorman.auth.model;


import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.newsletter.model.Newsletter;
import hu.fsmr.dorman.user.model.Inhabitant;
import hu.fsmr.dorman.utils.HashFunctions;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author blase
 * @version 0.1
 * @since 2016. 10. 16.
 */
@Entity
@Table(name = "auth_info")
public class User {
    //http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
    @Id
    @Column(name = "pk")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pk;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "inhab_id")
    private Inhabitant inhabitant;

    @Column(name="last_name")
    private String lastName;//vezeteknev

    @Column(name="first_name")
    private String firstName;//keresztnev

    @Column(name="read_neswletters")
    private String readNewsletters;

    public User() {}

    public static User login(String username, String password) {
        Session session = null;
        User user = null;
        try {
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(User.class)
                    .add(Restrictions.eq("username", username));
            user = (User) criteria.uniqueResult();
            //TODO nem letezo user
            if(!HashFunctions.validatePassword(user.getPassword(), password)){
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(session != null){
                session.close();
            }
        }
        return user;
    }

    public static User getUser(long pk) {
        Session session = null;
        User user = null;
        try {
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(User.class)
                    .add(Restrictions.eq("pk", pk));
            user = (User) criteria.uniqueResult();
            //TODO nem letezo user
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(session != null){
                session.close();
            }
        }
        return user;
    }

    public List<User> QueryAllAuthInfo() {
        Session session = null;
        List roles = null;
        try {
            roles = new ArrayList<User>();
            session = HibernateConfiguration.getSession();
            Criteria criteria = session.createCriteria(User.class);
            roles = criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return roles;
    }

    /**
     * Ures peldany, ha uj felhasznalot adunk hozza akkor az ebbe a peldanyba kerul bele.
     * @return
     */
    public User newAuthInfo() {
        return new User();
    }

    public void save() throws InvalidKeySpecException, NoSuchAlgorithmException {
        HibernateCommon.save(this);
    }

    public void save(String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        this.password = HashFunctions.generateStrongPasswordHash(password);
        HibernateCommon.save(this);
    }

    public void delete() throws Exception {
        deleteConnections();
        HibernateCommon.delete(this);
    }

    /**
     * Amig hozza van kapcsolva a role-hoz meg a user-hez nem lehet torolni a rekordot
     * ezert mielott torlom fel kell bontani a kapcsolatot
     */
    private void deleteConnections() {
        //this.role = null;
        //this.user = null;
    }

    public void addReadNewsletter(Newsletter newsletter)
    {
        readNewsletters +=","+newsletter.getPk();
    }


    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Inhabitant getInhabitant() {
        return inhabitant;
    }

    public void setInhabitant(Inhabitant inhabitant) {
        this.inhabitant = inhabitant;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getReadNewsletters() {
        return readNewsletters;
    }

    public void setReadNewsletters(String readNewsletters) {
        this.readNewsletters = readNewsletters;
    }

    @Override
    public String toString() {
        return String.valueOf(pk);
    }
}
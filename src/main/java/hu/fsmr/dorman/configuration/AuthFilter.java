package hu.fsmr.dorman.configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by blase on 2016. 10. 14..
 */
public class AuthFilter implements Filter{
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            HttpServletResponse res = (HttpServletResponse) servletResponse;
            HttpSession ses = req.getSession(false);
            String reqURI = req.getRequestURI();

            if ( reqURI.indexOf("/login.xhtml") >= 0 || (ses != null && ses.getAttribute("username") != null)
                    || reqURI.indexOf("/public/") >= 0 || reqURI.contains("javax.faces.resource") ) {
                filterChain.doFilter(servletRequest, servletResponse);
            }
            else{
                res.sendRedirect(req.getContextPath()+"/views/auth/login.xhtml");
            }
        }
        catch(Throwable t) {
            System.out.println( "exception "+t.getMessage());
        }
    }

    public void destroy() {
    }
}

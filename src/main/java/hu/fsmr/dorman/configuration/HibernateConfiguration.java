package hu.fsmr.dorman.configuration;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.auth.model.Role;
import hu.fsmr.dorman.erroreport.model.ErrorReport;
import hu.fsmr.dorman.finance.model.Finance;
import hu.fsmr.dorman.newsletter.model.Newsletter;
import hu.fsmr.dorman.rent.model.Inventory;
import hu.fsmr.dorman.rent.model.Key;
import hu.fsmr.dorman.user.model.Inhabitant;
import hu.fsmr.dorman.user.model.TempInhab;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import java.util.Properties;

/**
 * Created by blase on 11/14/16.
 */
public class HibernateConfiguration {
    private static final SessionFactory sessionFactory;

    static {
        try {
            Properties prop = new Properties();
            prop.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
            prop.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/dorman?createDatabaseIfNotExist=true&autoReconnect=true");
            prop.setProperty("hibernate.connection.username", "root");
            prop.setProperty("hibernate.connection.password", "grundig1");
            prop.setProperty("hibernate.connection.autocommit", "true");
            prop.setProperty("dialect", "org.hibernate.dialect.MySQLDialect");
            prop.setProperty("hibernate.hbm2ddl.auto", "update");
            //prop.setProperty("hibernate.hbm2ddl.auto", "create");
            prop.setProperty("hibernate.show_sql", "true");
            prop.setProperty("hibernate.cache.use_second_level_cache", "false");
            prop.setProperty("hibernate.hbm2ddl.import_files","import.sql");
            prop.setProperty("hibernate.dbcp.validationQuery","select 1");
            prop.setProperty("hibernate.dbcp.testOnBorrow","true");


            sessionFactory = new AnnotationConfiguration()
                    .addPackage("hu.fsmr.dorman.*")
                    .addProperties(prop)
                    .addAnnotatedClass(User.class)//auth
                    .addAnnotatedClass(Role.class)//auth
                    .addAnnotatedClass(Newsletter.class)//newsletter
                    .addAnnotatedClass(Inhabitant.class)
                    .addAnnotatedClass(Inventory.class)
                    .addAnnotatedClass(Key.class)
                    .addAnnotatedClass(ErrorReport.class)
                    .addAnnotatedClass(Finance.class)
                    .addAnnotatedClass(TempInhab.class)
                    .buildSessionFactory();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
    }
    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }
}
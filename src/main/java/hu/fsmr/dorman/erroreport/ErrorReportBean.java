package hu.fsmr.dorman.erroreport;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.erroreport.model.ErrorReport;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by blase on 2017.10.24..
 */
@ManagedBean(name="errorReportBean")
@SessionScoped
public class ErrorReportBean implements Serializable {

    @ManagedProperty(value="#{authBean.auth}")
    private User user;

    private List<ErrorReport> errorReports;
    private List<ErrorReport> filteredErrorReports;
    private ErrorReport selectedErrorReport;
    private Map<String,Integer> mStatuses;
    private List<Integer> statuses;


    public ErrorReportBean(){
        mStatuses = new HashMap<String, Integer>();
        mStatuses.put("unchecked",1);
        mStatuses.put("in progress",2);
        mStatuses.put("done",3);

        statuses = new ArrayList<Integer>(){};
        statuses.add(1);
        statuses.add(2);
        statuses.add(3);
        refresh();
    }

    @PostConstruct
    private void init() {
        System.out.println(user.getFirstName());
    }

    private void refresh(){
        errorReports = ErrorReport.getErrorReportList();
        selectedErrorReport = new ErrorReport();
        filteredErrorReports = errorReports;
    }

    public void saveErrorReport(){
         try{
            if(null != selectedErrorReport ) {
                selectedErrorReport.setInhabitant(user);
                selectedErrorReport.save();
                refresh();
            }
            else{
                System.out.println("asdasdasd");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void modifyReport(ErrorReport er){
        try {
            er.save();
            refresh();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String styleByState(int errorState){
        if(errorState == 0) {
            return "redColoredPanel";
        }
        if (errorState ==2){
            return "blueColoredPanel";
        }
        if(errorState == 3){
            return "greenColoredPanel";
        }
        return "";
    }

    public boolean visibleErrorReports(ErrorReport report)
    {
        System.out.println(user.getRole().getRoleName());
        if (report.getInhabitant().getPk() == user.getPk() || user.getRole().getRoleName().equals("porta")){
            return true;
        }
        return false;
    }

    public List<ErrorReport> getErrorReports() {
        return errorReports;
    }

    public void setErrorReports(List<ErrorReport> errorReports) {
        this.errorReports = errorReports;
    }

    public List<ErrorReport> getFilteredErrorReports() {
        return filteredErrorReports;
    }

    public void setFilteredErrorReports(List<ErrorReport> filteredErrorReports) {
        this.filteredErrorReports = filteredErrorReports;
    }

    public ErrorReport getSelectedErrorReport() {
        return selectedErrorReport;
    }

    public void setSelectedErrorReport(ErrorReport selectedErrorReport) {
        this.selectedErrorReport = selectedErrorReport;
    }

    public Map<String, Integer> getmStatuses() {
        return mStatuses;
    }

    public void setmStatuses(Map<String, Integer> mStatuses) {
        this.mStatuses = mStatuses;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Integer> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Integer> statuses) {
        this.statuses = statuses;
    }
}

package hu.fsmr.dorman.erroreport;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Created by blase on 2017.11.06..
 */
@FacesConverter(value = "errorStatusConverter")
public class StatusConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        System.out.println("getAsObject");
        if(s.equals("Ellenőrizetlen")){
            return 1;
        }
        if(s.equals("Folyamatban")){
            return 2;
        }
        if(s.equals("Kész")){
            return 3;
        }
        return Integer.valueOf(s);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        System.out.println("getAsString");
        if((Integer)o==1){
            System.out.println(o.toString()+ "Ellenorizetlen");
            return "Ellenőrizetlen";
        }
        if((Integer)o==2){
            System.out.println(o.toString()+ "Folyamatban");
            return "Folyamatban";
        }
        if((Integer)o==3){
            System.out.println(o.toString()+ "Kész");
            return "Kész";
        }
        return o.toString();
    }
}

package hu.fsmr.dorman.erroreport.model;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blase on 2017.10.24..
 */
@Entity
@Table(name = "error_report")
public class ErrorReport {

    @Id
    @Column(name="error_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int error_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pk")
    private User inhabitant;

    @Column(name="state")
    private int state;
    //1 unchecked
    //2 in progress
    //3 done

    @Column(name ="title")
    private String title;

    @Column(name="error_description")
    private String errorDescription;

    @Column(name="private_comment")
    private String privateComment;

    public ErrorReport() {
        title="";
        errorDescription = "";
        privateComment = "";
    }

    public ErrorReport(int state, String errorDescription, String title) {
        this.state = state;
        this.errorDescription = errorDescription;
        this.title = title;
    }

    public static List<ErrorReport> getErrorReportList()
    {
        List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
        Session session = null;
        try {
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(ErrorReport.class);
            errorReport = criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return errorReport;
    }

    public void save() throws Exception {
        HibernateCommon.save(this);
    }

    public void delete() throws Exception{
        HibernateCommon.delete(this);
    }

    public User getInhabitant() {
        return inhabitant;
    }

    public void setInhabitant(User inhabitant) {
        this.inhabitant = inhabitant;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    public int getError_id() {
        return error_id;
    }

    public void setError_id(int error_id) {
        this.error_id = error_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

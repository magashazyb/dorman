package hu.fsmr.dorman.finance;

import hu.fsmr.dorman.finance.model.Finance;
import hu.fsmr.dorman.utils.PDFGenerator;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.*;
import java.util.Date;
import java.util.List;

/**
 * Created by blase on 2017.11.08..
 */
@ManagedBean(name = "financeBean")
@SessionScoped
public class FinanceBean implements Serializable{

    private List<Finance> allFinanceInfo;
    private Finance selectedFinanceInfo;
    private Integer balance;
    private UploadedFile picture;
    private StreamedContent file;


    public FinanceBean(){
        balance = 0;
        refresh();
    }

    public void save(){
        selectedFinanceInfo.setCommitTime(new Date());
        selectedFinanceInfo.save();
        refresh();
    }

    public void fileUpload(FileUploadEvent event) throws IOException {
        ExternalContext extContext =
                FacesContext.getCurrentInstance().getExternalContext();
        //String path = extContext.getRealPath("//WEB-INF//files//" + event.getFile().getFileName());
        String path = extContext.getRealPath("//resources//finance//" + event.getFile().getFileName());
        File result = new File(path);
        System.out.println(path);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(result);

            byte[] buffer = new byte[6124];

            int bulk;
            InputStream inputStream = event.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }

            fileOutputStream.close();
            inputStream.close();
            selectedFinanceInfo.addImagePath(event.getFile().getFileName());
        }catch (Exception e){
            e.printStackTrace();
        }

        /*String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        System.out.println(path);
        String name = event.getFile().getFileName();
        String file_path = path + "resources"+File.separator+"finance"+File.separator + name;
        //String file_path =  name;
        File file = new File(file_path );
        System.out.println(file.getAbsolutePath());
        selectedFinanceInfo.addImagePath(name);
        InputStream is = event.getFile().getInputstream();
        OutputStream out = new FileOutputStream(file);
        byte buf[] = new byte[1024];
        int len;
        while ((len = is.read(buf)) > 0)
            out.write(buf, 0, len);
        is.close();
        out.close();*/
    }

    public void delete() {
        if (selectedFinanceInfo != null)
        {
            try {
                selectedFinanceInfo.delete();
                refresh();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void newFinanceInfo(){
        selectedFinanceInfo = new Finance();
    }

    public void refresh(){
        allFinanceInfo = Finance.queryAllFinanceInfo();
        balance = 0;
        calculateBalance();
    }

    private void calculateBalance() {
        for (Finance finance : allFinanceInfo) {
            balance += finance.getMoneyMovement();
        }
    }

    public void download(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        try {
            String contentType = "application/pdf;charset=utf-8";
            ec.responseReset();
            ec.setResponseContentType(contentType);
            ec.setResponseHeader("Content-Disposition", "attachment; filename=Kimutatas.pdf");

            OutputStream outputStream = ec.getResponseOutputStream();
            outputStream.write(new PDFGenerator("finance").createFinancePdf().toByteArray());
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<Finance> getAllFinanceInfo() {
        return allFinanceInfo;
    }

    public void setAllFinanceInfo(List<Finance> allFinanceInfo) {
        this.allFinanceInfo = allFinanceInfo;
    }

    public Finance getSelectedFinanceInfo() {
        return selectedFinanceInfo;
    }

    public void setSelectedFinanceInfo(Finance selectedFinanceInfo) {
        this.selectedFinanceInfo = selectedFinanceInfo;
    }

    public UploadedFile getPicture() {
        return picture;
    }

    public void setPicture(UploadedFile picture) {
        this.picture = picture;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }
}

package hu.fsmr.dorman.finance.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;

/**
 * Created by blase on 2017.11.08..
 */
@Entity
@Table(name = "finance")
public class Finance {

    @Id
    @Column(name = "pk")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pk;

    @Column(name = "title")
    private String title;

    @Column(name = "money_movement")
    private Integer moneyMovement;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name="commit_time")
    private Date commitTime;

    @Column(name = "comment")
    private String comment;

    @Transient //igy nem mappeli be a hibernate
    private List<String> images;

    public Finance(String tite, Integer moneyMovement, String imagePath) {
        this.title = tite;
        this.moneyMovement = moneyMovement;
        this.imagePath = imagePath;
        this.images = new ArrayList<String>();
    }

    public Finance(){
        this.title = "";
        this.moneyMovement = 0;
        this.imagePath = "";
        this.comment = "";
        this.images = new ArrayList<String>();
    }

    public static List<Finance> queryAllFinanceInfo() {
        Session session = null;
        List financialInfos = null;
        try {
            financialInfos = new ArrayList<Finance>();
            session = HibernateConfiguration.getSession();
            Criteria criteria = session.createCriteria(Finance.class);
            financialInfos = criteria.list();

            for (Object financialInfo : financialInfos) {
                ((Finance)financialInfo).images = extractImagePath(((Finance)financialInfo));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return financialInfos;
    }

    public void addImagePath( String imagePath )
    {
        this.imagePath += "," + imagePath;
    }

    private static List<String> extractImagePath(Finance finfo){
        String[] images = finfo.imagePath.split(",");

        if(images.length <= 1 ){
            return new ArrayList<String>();
        }

        List<String> imagesList = new ArrayList<String>();
        for (String image : images) {
            if(!image.isEmpty() && !"".equals(image)){
                imagesList.add(image);
                System.out.println("image: "+image);
            }
        }
        System.out.println(imagesList.size());
        return imagesList;

    }

    public void save() {
        HibernateCommon.save(this);
    }

    public void delete() throws Exception {
        HibernateCommon.delete(this);
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public Integer getMoneyMovement() {
        return moneyMovement;
    }

    public void setMoneyMovement(Integer moneyMovement) {
        this.moneyMovement = moneyMovement;
    }


    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(Date commitTime) {
        this.commitTime = commitTime;
    }
}

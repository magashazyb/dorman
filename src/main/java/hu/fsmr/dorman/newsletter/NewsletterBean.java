package hu.fsmr.dorman.newsletter;

import hu.fsmr.dorman.newsletter.model.Newsletter;
import hu.fsmr.dorman.user.UserBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * Created by blase on 2017.10.08..
 */
@ManagedBean(name= "newsletterBean")
@SessionScoped
public class NewsletterBean {

    @ManagedProperty(value="#{userBean}")
    private UserBean userBean;

    private List<Newsletter> newsletters;
    private List<Newsletter> filteredNewsletters;
    private Newsletter selectedNewsletter;

    public NewsletterBean(){
        newNewsletter();
        refresh();
    }

    private void refresh(){
        newsletters = Newsletter.getNewsletters();
        filteredNewsletters = newsletters;
    }

    public void saveNewsletter(){
        try{
            selectedNewsletter.save();
            newNewsletter();
            refresh();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void newNewsletter(){
        selectedNewsletter = new Newsletter();
    }

    //beállítjuk a cikket már olvasottnak
    public void alreadyRead(Newsletter newsletter)
    {
        try {
            userBean.getUser().addReadNewsletter(newsletter);
            userBean.getUser().save();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    //lekérdezzük már olvasott-e a cikk
    /*public boolean queryToogle(Newsletter newsletter)
    {
        System.out.println("olvasott-e"+userBean.getUser().getReadNewsletters());
        String alreadyRead = userBean.getUser().getReadNewsletters();
        if (null == alreadyRead){
            return false;
        }
        String[] split = alreadyRead.split(",");
        for (String s : split) {
            if( String.valueOf(newsletter.getPk()).equals(s)){
                return true;
            }
        }
        return false;
    }*/

    public boolean queryToogle(){
        return true;
    }

    public void deleteNewsletter(){
        try{
            selectedNewsletter.delete();
            refresh();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Newsletter> getNewsletters() {
        refresh();
        return newsletters;
    }

    public void setNewsletters(List<Newsletter> newsletters) {
        this.newsletters = newsletters;
    }

    public List<Newsletter> getFilteredNewsletters() {
        return filteredNewsletters;
    }

    public void setFilteredNewsletters(List<Newsletter> filteredNewsletters) {
        this.filteredNewsletters = filteredNewsletters;
    }

    public Newsletter getSelectedNewsletter() {
        return selectedNewsletter;
    }

    public void setSelectedNewsletter(Newsletter selectedNewsletter) {
        this.selectedNewsletter = selectedNewsletter;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }
}

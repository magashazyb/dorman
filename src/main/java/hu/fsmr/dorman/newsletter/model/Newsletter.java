package hu.fsmr.dorman.newsletter.model;

import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blase on 2017.10.08..
 */
@Entity
@Table(name = "newsletter")
public class Newsletter {

    @Id
    @Column(name="pk")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pk;

    @Column(name="author")
    private String author;

    @Column(name="title")
    private String title;

    @Column(name="content")
    private String content;//vezeteknev

    @Column(name="visible")
    private boolean visible;//keresztnev

    public Newsletter() {
        author = new String();
        title = new String();
        content = new String();
        visible = true;
    }

    public Newsletter(String author, String title, String content, boolean visible) {
        this.author = author;
        this.title = title;
        this.content = content;
        this.visible = visible;
    }

    public static List<Newsletter> getNewsletters() {
        List<Newsletter> newsletter = new ArrayList<Newsletter>();
        Session session = null;
        try {
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(Newsletter.class);
            newsletter = criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return newsletter;
    }

    public void save() throws Exception {
        HibernateCommon.save(this);
    }

    public void delete() throws Exception{
        HibernateCommon.delete(this);
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Newsletter{" +
                "pk=" + pk +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", visible=" + visible +
                '}';
    }
}

package hu.fsmr.dorman.rent;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.rent.model.Inventory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;

/**
 * Created by blase on 2017.10.09..
 */
@ManagedBean(name= "inventoryBean")
@SessionScoped
public class InventoryBean {

    @ManagedProperty(value = "#{authBean.auth}")
    private User user;

    private List<Inventory> rentableItems;//osszes


    private Inventory selectedItem;

    public InventoryBean() {
        refresh();
    }


    public void newInventoryItem() {
        selectedItem = new Inventory();
    }

    public void refresh() {
        rentableItems = Inventory.getInventoryItems();
    }

    public void save() {
        selectedItem.save();
        refresh();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Inventory> getRentableItems() {
        return rentableItems;
    }

    public void setRentableItems(List<Inventory> rentableItems) {
        this.rentableItems = rentableItems;
    }

    public Inventory getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Inventory selectedItem) {
        this.selectedItem = selectedItem;
    }

}

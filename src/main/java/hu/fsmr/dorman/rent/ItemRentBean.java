package hu.fsmr.dorman.rent;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.rent.model.Inventory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * Created by blase on 2017.11.23..
 */
@ManagedBean(name= "itemRentBean")
@SessionScoped
public class ItemRentBean {
    @ManagedProperty(value = "#{authBean.auth}")
    private User user;

    @ManagedProperty(value = "#{userBean.selectedUser}")
    private User selectedUser;

    private List<Inventory> rentableItems;//osszes

    private String[] selectedRentItems;//kivalasztott


    @PostConstruct
    public void init(){
        selectedUser = user;//dirty hack..valakinek lennie kell a selected usernek h a bean toltesekor letezzenek a peldanyok de ezt ugyis felul irjuk
        refresh();
    }

    private String[] convertSelectedItems(){
        String[] ret = new String[rentableItems.size()];
        String rentedItems=null;// = selectedUser.getInhabitant().getRentedItems();
        if( rentedItems == null ){
            return  ret;
        }

        if( rentedItems.startsWith(",")){
            rentedItems = rentedItems.substring(1);
        }

        String[] split =  rentedItems.split(",");

        for (int i = 0; i < split.length; i++) {
            ret[i] = split[i];
        }

        return ret;
    }

    public void loadUserBean(){
        System.out.println(selectedUser.getFirstName());
    }

    public void refresh(){
        System.out.println("refresh run");
        System.out.println(selectedUser.getFirstName());
        rentableItems = Inventory.getInventoryItems();
        selectedRentItems = convertSelectedItems();
        System.out.println(selectedUser.getInhabitant().getRentedItems());
    }

    public String getRentedItems(){
        String rentedItems = "";
        for (String selectedRentItem : selectedRentItems) {
            rentedItems +="," + selectedRentItem;
        }
        return rentedItems;
    }

    public void save() {
        try {
            //selectedUser.getInhabitant().setRentedItems(getRentedItems());
            selectedUser.save();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void testSelected(){
        for (String selectedRentItem : selectedRentItems) {
            System.out.println(selectedRentItem);
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Inventory> getRentableItems() {
        return rentableItems;
    }

    public void setRentableItems(List<Inventory> rentableItems) {
        this.rentableItems = rentableItems;
    }

    public String[] getSelectedRentItems() {
        return selectedRentItems;
    }

    public void setSelectedRentItems(String[] selectedRentItems) {
        this.selectedRentItems = selectedRentItems;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }


}

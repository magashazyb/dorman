package hu.fsmr.dorman.rent;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.rent.model.Key;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.Calendar;
import java.util.List;

/**
 * Created by blase on 2017.11.13..
 */
@ManagedBean(name="keyBean")
@SessionScoped
public class KeyBean{
    private List<Key> keys;
    private Key selectedKey;
    private String selectedUserPk;

    public KeyBean(){
        refresh();
    }

    public void refresh(){
        keys = Key.getKeys();
    }

    public void newKey(){
        selectedKey = new Key();
    }

    public void save(){
        selectedKey.setRented(false);
        selectedKey.save();
        refresh();
    }

    public void rentKey(){
        selectedKey.setRenter(User.getUser(Long.valueOf(selectedUserPk)));
        selectedKey.setRentStart(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        selectedKey.setRented(true);
        selectedKey.save();
        refresh();
    }

    public void returnKey(){
        selectedKey.setRented(false);
        selectedKey.save();
        refresh();
    }

    public String styleByRentState(Key k){
        if(k.isRented()){
            return "redColoredPanel";
        }
        else {
            return "greenColoredPanel";
        }
    }
    public List<Key> getKeys() {
        return keys;
    }

    public void setKeys(List<Key> keys) {
        this.keys = keys;
    }

    public Key getSelectedKey() {
        return selectedKey;
    }

    public void setSelectedKey(Key selectedKey) {
        this.selectedKey = selectedKey;
    }

    public String getSelectedUserPk() {
        return selectedUserPk;
    }

    public void setSelectedUserPk(String selectedUserPk) {
        this.selectedUserPk = selectedUserPk;
    }
}

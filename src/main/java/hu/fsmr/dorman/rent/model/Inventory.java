package hu.fsmr.dorman.rent.model;

import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blase on 2017.11.08..
 */
@Entity
@Table(name="inventory")
public class Inventory {

    @Id
    @Column(name="inventory_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int inventoryId;

    @Column(name="item_name")
    private String itemName;

    @Column(name="item_count")
    private String itemCount;

    @Column(name="item_comment")
    private String itemComment;


    public static List<Inventory> getInventoryItems() {
        Session session = null;
        List inventoryItems = null;
        try {
            inventoryItems = new ArrayList<Inventory>();
            session = HibernateConfiguration.getSession();
            Criteria criteria = session.createCriteria(Inventory.class);
            inventoryItems = criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return inventoryItems;
    }

    public static Inventory getInventoryItemById( int id ) {
        Session session = null;
        Inventory inv = null;
        try {
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(Inventory.class)
                    .add(Restrictions.eq("inventoryId", id));
            inv = (Inventory) criteria.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(session != null){
                session.close();
            }
        }
        return inv;
    }

    public void save() {
        HibernateCommon.save(this);
    }

    public void delete() throws Exception {
        HibernateCommon.delete(this);
    }

    public int getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(int inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemComment() {
        return itemComment;
    }

    public void setItemComment(String itemComment) {
        this.itemComment = itemComment;
    }


    @Override
    public String toString() {
        return String.valueOf(inventoryId);
    }
}


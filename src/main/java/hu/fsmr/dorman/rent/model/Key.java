package hu.fsmr.dorman.rent.model;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by blase on 2017.11.20..
 */
@Entity
@Table(name="key_rent")
public class Key {

    @Id
    @Column(name="key_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int keyId;

    @Column(name="key_name")
    private String keyName;

    @Column(name="key_comment")
    private String keyComment;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pk")
    private User renter;

    @Column(name= "rent_start")
    private Date rentStart;

    @Column(name="rented")
    private boolean rented;

    public Key(){}

    public Key(String keyName, String itemComment, Date rentStart, User renter) {
        this.keyName = keyName;
        this.keyComment = itemComment;
        this.rentStart = rentStart;
    }

    public static List<Key> getKeys(){
        Session session = null;
        List rendatbleKeys = null;
        try {
            rendatbleKeys = new ArrayList<Inventory>();
            session = HibernateConfiguration.getSession();
            Criteria criteria = session.createCriteria(Key.class);
            rendatbleKeys = criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return rendatbleKeys;
    }

    public void save() {
        HibernateCommon.save(this);
    }

    public int getKeyId() {
        return keyId;
    }

    public void setKeyId(int keyId) {
        this.keyId = keyId;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyComment() {
        return keyComment;
    }

    public void setKeyComment(String keyComment) {
        this.keyComment = keyComment;
    }

    public Date getRentStart() {
        return rentStart;
    }

    public void setRentStart(Date rentStart) {
        this.rentStart = rentStart;
    }


    public User getRenter() {
        return renter;
    }

    public void setRenter(User renter) {
        this.renter = renter;
    }

    public boolean isRented() {
        return rented;
    }

    public void setRented(boolean rented) {
        this.rented = rented;
    }

    @Override
    public String toString() {
        return "Key{" +
                "keyId=" + keyId +
                ", keyName='" + keyName + '\'' +
                ", keyComment='" + keyComment + '\'' +
                ", rentStart=" + rentStart +
                '}';
    }
}

package hu.fsmr.dorman.roomallocation;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.user.Sort;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import sun.reflect.generics.tree.Tree;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.*;

/**
 * Created by blase on 2017.11.07..
 */
@ManagedBean(name="roomAllocBean")
@ViewScoped
public class RoomAllocBean {
    //CSINÁLNI EGY MAPET AMIBE TREENODEROOMNUMBER KEYYEL ELRAKNI EGY SELECTION TREENODEOT AZTÁN ABBÓL LEKÉRDEZNI
    @ManagedProperty(value = "#{authBean.auth}")
    private User user;

    private List<TreeNode> roomAllocList;
    private TreeNode selectedNode;
    private List<TreeNode> selectedList;
    private Map<Integer, Map<String, User>> roomMap;
    private Sort roomSort;
    private List<User> users;
    private String temp = "hello";

    public RoomAllocBean(){
    }

    @PostConstruct
    public void init(){
        System.out.println("wat?");
        users = user.QueryAllAuthInfo();
        roomSort = new Sort(users);
        roomMap = roomSort.getRoom();
        roomAllocList = createRoomInfo(roomMap);
        selectedList = roomAllocList;
        selectedNode = new DefaultTreeNode();
    }

    public void refresh(){
        users = user.QueryAllAuthInfo();
        roomSort = new Sort(users);
        roomMap = roomSort.getRoom();
        roomAllocList = createRoomInfo(roomMap);
    }

    public TreeNode getSelection(){
        return new DefaultTreeNode("",null);
    }

    public void writeOut(){
        for (TreeNode treeNode : roomAllocList) {
            TreeNode tn = treeNode;
            System.out.println(tn.getData().toString());
            List<TreeNode> tnc = tn.getChildren();
            for (TreeNode node : tnc) {
                System.out.println(node.getData().toString());
            }
            System.out.println();
        }
    }


    private List<TreeNode> createRoomInfo(Map<Integer, Map<String, User>> users){
        List<TreeNode> rooms = new ArrayList<TreeNode>();
        Iterator roomIterator = users.keySet().iterator();
        System.out.println("createroomInfo");

        while(roomIterator.hasNext()){
            int roomNumber = (Integer)roomIterator.next();
            System.out.println("szoba: "+roomNumber);
            TreeNode roomNode = new DefaultTreeNode("Szoba "+roomNumber, null);

            //Map room = users.get(roomIterator.next());
            Map room = users.get(roomNumber);
            Iterator inhabIT = room.keySet().iterator();
            while(inhabIT.hasNext()){
                User user = (User)room.get(inhabIT.next());
                if( null != user ){
                    TreeNode userNode = new DefaultTreeNode(user, roomNode);
                    System.out.println(user);
                }
            }
            rooms.add(roomNode);
            System.out.println();
        }

        return rooms;
    }



    public void onDragDrop(TreeDragDropEvent event) {
        System.out.println("TDD EVENT___________________");
        System.out.println(event.toString());
        /*TreeNode dragNode = event.getDragNode();
        TreeNode dropNode = event.getDropNode();

        System.out.println(dragNode.getData());
        System.out.println(dropNode.getData());
        if (!dropNode.getData().toString().toLowerCase().startsWith("Szoba")){
            refresh();
            System.out.println("invalid");
        }
        System.out.println();

        System.out.println(dropNode.getType());
        int dropIndex = event.getDropIndex();

        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Dragged " + dragNode.getData(), "Dropped on " + dropNode.getData() + " at " + dropIndex);
        FacesContext.getCurrentInstance().addMessage(null, message);*/
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<TreeNode> getRoomAllocList() {
        return roomAllocList;
    }

    public void setRoomAllocList(List<TreeNode> roomAllocList) {
        this.roomAllocList = roomAllocList;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}


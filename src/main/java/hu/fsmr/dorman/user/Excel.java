package hu.fsmr.dorman.user;

import hu.fsmr.dorman.auth.model.Role;
import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.user.model.Inhabitant;
import hu.fsmr.dorman.utils.HashFunctions;
import org.apache.poi.ss.usermodel.*;


import java.io.File;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by blase on 2017.10.24..
 */

public class Excel {

    private String path = "";


    public static void main(String[] args) {
        new Excel("/home/blase/Prog/java/szakdolgozat/DormManager/felvettek.xlsx");
    }


    public Excel( String path) {
        this.path = path;
    }

    public Excel(){}

    public List<User> processExcel( InputStream is ) {
        List<User> excelContent = new ArrayList<User>();
        try {
            File myFile = new File(path);
            Workbook myWorkBook = WorkbookFactory.create(is);

            Sheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIterator = mySheet.iterator();

            for (int i = 1; i < mySheet.getLastRowNum()-2; i++) {
                Row row = mySheet.getRow(i);
                String[] name = row.getCell(1).getStringCellValue().split(" ");
                User u = new User();
                u.setFirstName(name[0]);
                u.setLastName(name[1]);
                if (null != row.getCell(0)) {

                    Inhabitant inhabitant = new Inhabitant();
                    inhabitant.setNeptunCode(row.getCell(0).getStringCellValue());
                    inhabitant.setGender(getCellContent(row.getCell(2)));
                    inhabitant.setMajor(getCellContent(row.getCell(3)));
                    inhabitant.setPermaAddress(getAddress(row));
                    inhabitant.setEmailAddress(getCellContent(row.getCell(8)));
                    inhabitant.setPermaPhoneNumber(getCellContent(row.getCell(9)));
                    inhabitant.setClassYear(getCellContent(row.getCell(11)));
                    u.setInhabitant(inhabitant);
                    //van neptun kod tehat nem alberlo tehat letrehozzuk neki az inhabitant adatat is
                }
                u.setRole(Role.getRoleByName("user"));
                String uname = Normalizer.normalize(name[0], Normalizer.Form.NFD) + i;
                u.setUsername(uname);
                u.setPassword(HashFunctions.generateStrongPasswordHash(uname));
                excelContent.add(u);
            }
            System.out.println(mySheet.getLastRowNum());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return excelContent;
    }

    private String getAddress(Row r) {

        return getCellContent(r.getCell(4))+" "//orszag
                + getCellContent(r.getCell(5))+" "//irszam
                + getCellContent(r.getCell(6))+" "//telepules
                + getCellContent(r.getCell(7));//cim
    }

    private String getCellContent(Cell c) {
        String returnVal = "";

        if (null == c) {
            return returnVal;
        }

        switch (c.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                returnVal = c.getStringCellValue();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                returnVal = String.valueOf((int)c.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                returnVal = String.valueOf(c.getBooleanCellValue());
                break;
            default:
        }
        return returnVal;

    }

}

package hu.fsmr.dorman.user;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.user.model.Inhabitant;

import java.util.*;

/**
 * Created by blase on 2017.11.21..
 */
public class Sort {

    //abból indulunk ki h nem lesz több hallgató mint férőhely
    private int roomSize = 3;
    private int hallgatokszama = 0;

    /*
    egyik map szak szerint
    másik map évek szerint
    megnézzük a közös elemeket abbol kiveszünk kettőt
    aztán mellé rakunk egy friss embert
     */

    private Map<String, User> ANMap;//AMK Nik
    private Map<String, User> AKMap;//AMK Kandó
    private Map<String, User> AMMap;//AMK Manager

    private Map<String, Map<String, User>> yearMap;
    private List<User> remain;


    private Map<String, Map<String, User>> sortedAN;
    private Map<String, Map<String, User>> sortedAK;
    private Map<String, Map<String, User>> sortedAM;

    private Map<Integer, Map<String, User>> room;

    private List<User> userList;

    public Sort(List<User> usersList) {
        this.userList = usersList;
        this.ANMap = new HashMap<String, User>();//AMK Nik
        this.AKMap = new HashMap<String, User>();//AMK Kandó
        this.AMMap = new HashMap<String, User>();//AMK Manager

        this.yearMap = new TreeMap<String, Map<String, User>>();
        this.remain = new ArrayList<User>();

        this.sortedAN = new TreeMap<String, Map<String, User>>();
        this.sortedAK = new TreeMap<String, Map<String, User>>();
        this.sortedAM = new TreeMap<String, Map<String, User>>();

        this.room = new TreeMap<Integer, Map<String, User>>();

        room = getRoomList();
    }

    private Map<Integer, Map<String, User>> getRoomList() {
        sortUsersByMajor();//szak szerint sortoljuk az embereket
        //itt most megvannak évekve lebontva az azonos karra tartozó hallgatók
        sortedAN = getInhabsYearOrder(ANMap, yearMap);
        sortedAK = getInhabsYearOrder(AKMap, yearMap);
        sortedAM = getInhabsYearOrder(AMMap, yearMap);

        return createRoomMap();
    }

    private Map<Integer, Map<String, User>> createRoomMap() {
        Map<Integer, Map<String, User>> room = new TreeMap<Integer, Map<String, User>>();
        int roomCounter = 1;
        Map<Integer, Map<String, User>> returnList = createSortedList(roomCounter, sortedAN);
        roomCounter += returnList.size() + 1;
        room.putAll(returnList);

        returnList = createSortedList(roomCounter, sortedAM);
        roomCounter += returnList.size() + 1;
        room.putAll(returnList);

        returnList = createSortedList(roomCounter, sortedAK);
        roomCounter += returnList.size() + 1;
        room.putAll(returnList);
        return room;
    }

    private void sortUsersByMajor() {
        for (User user : userList) {
            if (null != user.getInhabitant() && null != user.getInhabitant().getMajor() ) {
                try {
                    String neptun = user.getInhabitant().getNeptunCode();
                    if (user.getInhabitant().getMajor().equals("AN")) {
                        ANMap.put(neptun, user);
                    }

                    if (user.getInhabitant().getMajor().equals("AK")) {
                        AKMap.put(neptun, user);
                    }

                    if (user.getInhabitant().getMajor().equals("AM")) {
                        AMMap.put(neptun, user);
                    }
                } catch (Exception e) {
                    System.out.println(user);
                    e.printStackTrace();
                }
                addUserToYearMap(user);
            } else {
                remain.add(user);
            }
        }
    }

    private void addUserToYearMap(User user) {
        try {
            String year = user.getInhabitant().getClassYear();
            if (null == yearMap.get(year)) {
                yearMap.put(year, new HashMap<String, User>());
            }

            String neptun = user.getInhabitant().getNeptunCode();
            Map<String, User> userMap = yearMap.get(year);
            userMap.put(neptun, user);
            yearMap.put(year, userMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



        private Map<Integer, Map<String, User>> createSortedList(Integer roomStartPoint, Map<String, Map<String, User>> sortedUsers) {
        Map<Integer, Map<String, User>> room = new TreeMap<Integer, Map<String, User>>();
        Iterator it = sortedUsers.entrySet().iterator();
        int roomCounter = roomStartPoint;
        while (it.hasNext()) {
            Map.Entry baseYear = (Map.Entry) it.next();
            Hashtable<String, User> base = (Hashtable<String, User>) baseYear.getValue();
            Hashtable<String, User> next = null;

            if (it.hasNext()) {
                Map.Entry nextYear = (Map.Entry) it.next();
                next = (Hashtable<String, User>) nextYear.getValue();
            }

            Iterator baseIt = base.entrySet().iterator();
            Iterator nextIt = next.entrySet().iterator();

            while (baseIt.hasNext()) {
                User oldUser1 = (User) ((Map.Entry) baseIt.next()).getValue();
                User oldUser2 = null;
                User newUser = null;
                if (baseIt.hasNext()) {
                    oldUser2 = (User) ((Map.Entry) baseIt.next()).getValue();
                }

                if (nextIt.hasNext()) {
                    newUser = (User) ((Map.Entry) nextIt.next()).getValue();
                }
                HashMap<String, User> temp = new HashMap<String, User>();
                temp.put("1", oldUser1);
                temp.put("2", oldUser2);
                temp.put("3", newUser);
                room.put(roomCounter, temp);

                roomCounter++;
                if(!baseIt.hasNext()){
                    baseIt = nextIt;
                    if( it.hasNext()){
                        Map.Entry nextYear = (Map.Entry) it.next();
                        next = (Hashtable<String, User>) nextYear.getValue();
                        nextIt = next.entrySet().iterator();
                    }
                }
            }
        }
        return room;
    }


    private Map<String, Map<String, User>> getInhabsYearOrder(Map<String, User> mayorMap, Map<String, Map<String, User>> yearMap) {
        Map<String, Map<String, User>> sorted = new TreeMap<String, Map<String, User>>();
        for (String s : yearMap.keySet()) {
            if (null == sorted.get(s)) {
                sorted.put(s, new HashMap<String, User>());
            }

            Hashtable<String, User> inersect = new Hashtable<String, User>(mayorMap);
            inersect.keySet().retainAll(yearMap.get(s).keySet());
            sorted.put(s, inersect);
        }
        return sorted;
    }

    public String getRoomNumberByPk(long pk){
        String roomNumber="";
        Iterator roomIterator = room.entrySet().iterator();
        while( roomIterator.hasNext() ){
            Map.Entry roomEntry = (Map.Entry ) roomIterator.next();
            roomNumber = ((Integer)roomEntry.getKey()).toString();
            Map room = (HashMap<String, User>) roomEntry.getValue();
            Iterator userIterator = room.entrySet().iterator();
            while(userIterator.hasNext()){
                Map.Entry userEntry = (Map.Entry ) userIterator.next() ;
                User u = ((User)userEntry.getValue());
                if( null != u ){
                    if( u.getPk() == pk){
                        return roomNumber;
                    }
                }
            }
        }
        return null;
    }

    public Map<Integer, Map<String, User>> getRoom() {
        return room;
    }

    public static void main(String[] args) {
        try {
            User ai = User.login("admin", "..");
            List<User> userList = ai.QueryAllAuthInfo();
            Sort s = new Sort(userList);
            Map<Integer, Map<String, User>> rommMap = s.getRoomList();
            Iterator rIt = s.getRoomList().keySet().iterator();
            while(rIt.hasNext()){
                Map room = rommMap.get(rIt.next());
                Iterator inhabIT = room.keySet().iterator();
                while(inhabIT.hasNext()){
                    User user = (User)room.get(inhabIT.next());
                    System.out.println(user);
                }
                System.out.println();
                //User u = ((User)userEntry.getValue());
            }
            //System.out.println(s.getRoomList());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

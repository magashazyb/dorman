package hu.fsmr.dorman.user;

import hu.fsmr.dorman.user.model.TempInhab;
import hu.fsmr.dorman.utils.PDFGenerator;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by blase on 2017.11.22..
 */
@ManagedBean(name="tempImhabBean")
@SessionScoped
public class TempInhabBean {
    private List<TempInhab> tempInhabList;
    private TempInhab selectedTInhab;
    private String residentInhabPK;

    public TempInhabBean(){
        refresh();
    }

    public void download(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        try {
            String contentType = "application/pdf;charset=utf-8";
            ec.responseReset();
            ec.setResponseContentType(contentType);
            ec.setResponseHeader("Content-Disposition", "attachment; filename="+selectedTInhab.getName()+".pdf");

            OutputStream outputStream = ec.getResponseOutputStream();
            outputStream.write(new PDFGenerator("tInhab").createTInhabPdf(selectedTInhab).toByteArray());
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }


    public void refresh(){
        tempInhabList = TempInhab.getTempInhabitants();
    }

    public void newTempInhab() {
        selectedTInhab = new TempInhab();
    }

    public void save(){
        selectedTInhab.save();
        refresh();
    }

    public List<TempInhab> getTempInhabList() {
        return tempInhabList;
    }

    public void setTempInhabList(List<TempInhab> tempInhabList) {
        this.tempInhabList = tempInhabList;
    }

    public TempInhab getSelectedTInhab() {
        return selectedTInhab;
    }

    public void setSelectedTInhab(TempInhab selectedTInhab) {
        this.selectedTInhab = selectedTInhab;
    }

    public String getResidentInhabPK() {
        return residentInhabPK;
    }

    public void setResidentInhabPK(String residentInhabPK) {
        this.residentInhabPK = residentInhabPK;
    }
}

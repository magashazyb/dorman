package hu.fsmr.dorman.user;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.auth.model.Role;
import hu.fsmr.dorman.rent.model.Inventory;
import hu.fsmr.dorman.roomallocation.RoomAllocBean;
import hu.fsmr.dorman.roomallocation.model.RoomAlloc;
import hu.fsmr.dorman.user.model.Inhabitant;
import hu.fsmr.dorman.utils.PDFGenerator;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * @author blase
 * @version ${VERSION}
 * @since 11/17/16
 */
@ManagedBean(name="userBean")
@SessionScoped
public class UserBean {

    @ManagedProperty(value="#{authBean.auth}")
    private User user;

    private Sort sort;

    private List<User> userList;
    private List<User> filteredUserList;

    private User selectedUser;
    private Role selectedRole;

    private String newPassword;

    private List<Inventory> rentableItems;//osszes

    @PostConstruct
    public void init(){
        selectedUser = user;
        refresh();
    }

    public void refresh(){
        userList = user.QueryAllAuthInfo();
        rentableItems = Inventory.getInventoryItems();
        selectedRole = selectedUser.getRole();
        newPassword = "";
    }

    public void save() throws Exception{
        if( !newPassword.equals("")) {
            System.out.println("newPassword "+newPassword);
            //selectedUser.setPassword(newPassword);
            selectedUser.save(newPassword);
        }
        System.out.println("nincs uj jelszo");
        selectedUser.save();
        refresh();
    }

    public void delete() throws Exception{
        System.out.println(selectedUser.toString());
        selectedUser.setRole(null);
        selectedUser.delete();
        refresh();
    }

    public void inventoryRefresh(){
        rentableItems = Inventory.getInventoryItems();
    }

    public void download(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        try {
            String contentType = "application/pdf;charset=utf-8";
            ec.responseReset();
            ec.setResponseContentType(contentType);
            ec.setResponseHeader("Content-Disposition", "attachment; filename="+selectedUser.getFirstName()+" "+selectedUser.getLastName()+".pdf");

            OutputStream outputStream = ec.getResponseOutputStream();
            outputStream.write(new PDFGenerator("inhab").createInhabPdf(selectedUser).toByteArray());
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }

    }

    public void addExcelUsers(FileUploadEvent event) {
        try {
            if(null != event && null != event.getFile() ){
                List<User> newUsers = new Excel().processExcel(event.getFile().getInputstream());
                for (User newUser : newUsers) {
                    newUser.save();
                }
            }
            addRooms();
        }catch (Exception e) {
            e.printStackTrace();
        }
        refresh();
    }

    private void addRooms() throws InvalidKeySpecException, NoSuchAlgorithmException {
        List<User> newUsers = user.QueryAllAuthInfo();
        sort = new Sort(newUsers);
        for (User newUser : newUsers) {
            String roomNumber = sort.getRoomNumberByPk(newUser.getPk());

            if(null != roomNumber ) {
                newUser.getInhabitant().setRoomNumber(roomNumber);
                newUser.save();
            }
        }
    }

    public void newUser(){
        selectedUser = new User();
        selectedUser.setInhabitant(new Inhabitant());
    }

    public void newInhabitant(){
        if (null ==selectedUser.getInhabitant()){
            selectedUser.setInhabitant(new Inhabitant());
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public Role getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Role selectedRole) {
        this.selectedRole = selectedRole;
    }

    public List<User> getFilteredUserList() {
        return filteredUserList;
    }

    public void setFilteredUserList(List<User> filteredUserList) {
        this.filteredUserList = filteredUserList;
    }


    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public List<Inventory> getRentableItems() {
        return rentableItems;
    }

    public void setRentableItems(List<Inventory> rentableItems) {
        this.rentableItems = rentableItems;
    }

}

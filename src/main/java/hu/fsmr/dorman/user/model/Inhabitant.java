package hu.fsmr.dorman.user.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by blase on 2016.12.18..
 */
@Entity
@Table(name = "inhabitant")
public class Inhabitant {

    @Id
    @Column(name = "inhab_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long inhabId;


    @Column(name="neptun_code")
    private String neptunCode;

    @Column(name="gender")
    private String gender;

    @Column(name="birth_date")
    private Date birthDate;

    @Column(name="mother_name")
    private String motherName;

    @Column(name="perma_phone_number")
    private String permaPhoneNumber;

    @Column(name="wired_phone_number")
    private String wiredPhoneNumber;

    @Column(name="parent_phone_number")
    private String parentPhoneNumber;

    @Column(name="perma_address")
    private String permaAddress;

    @Column(name="email_address")
    private String emailAddress;

    @Column(name="major")
    private String major;

    @Column(name="class_year")
    private String classYear;

    @Column(name="room_number")
    private String roomNumber;

    @Column(name="rfid_code")
    private String rfidCode;

    @Column(name="move_in_date")
    private Date moveInDate;

    @Column(name="move_out_date")
    private Date moveOutDate;

    @Column(name="comment")
    private String comment;

    @Column(name="rented_items")
    private String[] rentedItems;

    @Column(name="visible")
    private boolean visible;


    public long getInhabId() {
        return inhabId;
    }

    public void setInhabId(long inhabId) {
        this.inhabId = inhabId;
    }

    public String getNeptunCode() {
        return neptunCode;
    }

    public void setNeptunCode(String neptunCode) {
        this.neptunCode = neptunCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getPermaPhoneNumber() {
        return permaPhoneNumber;
    }

    public void setPermaPhoneNumber(String permaPhoneNumber) {
        this.permaPhoneNumber = permaPhoneNumber;
    }

    public String getWiredPhoneNumber() {
        return wiredPhoneNumber;
    }

    public void setWiredPhoneNumber(String wiredPhoneNumber) {
        this.wiredPhoneNumber = wiredPhoneNumber;
    }

    public String getParentPhoneNumber() {
        return parentPhoneNumber;
    }

    public void setParentPhoneNumber(String parentPhoneNumber) {
        this.parentPhoneNumber = parentPhoneNumber;
    }

    public String getPermaAddress() {
        return permaAddress;
    }

    public void setPermaAddress(String permaAddress) {
        this.permaAddress = permaAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getClassYear() {
        return classYear;
    }

    public void setClassYear(String classYear) {
        this.classYear = classYear;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRfidCode() {
        return rfidCode;
    }

    public void setRfidCode(String rfidCode) {
        this.rfidCode = rfidCode;
    }

    public Date getMoveInDate() {
        return moveInDate;
    }

    public void setMoveInDate(Date moveInDate) {
        this.moveInDate = moveInDate;
    }

    public Date getMoveOutDate() {
        return moveOutDate;
    }

    public void setMoveOutDate(Date moveOutDate) {
        this.moveOutDate = moveOutDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String[] getRentedItems() {
        return rentedItems;
    }

    public void setRentedItems(String[] rentedItems) {
        this.rentedItems = rentedItems;
    }

    @Override
    public String toString() {
        return "Inhabitant{" +
                "neptunCode='" + neptunCode + '\'' +
                ", major='" + major + '\'' +
                ", classYear='" + classYear + '\'' +
                '}';
    }
}

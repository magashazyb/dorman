package hu.fsmr.dorman.user.model;

import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.configuration.HibernateConfiguration;
import hu.fsmr.dorman.utils.HibernateCommon;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by blase on 2017.11.22..
 */
@Entity
@Table(name = "temp_inhabitant")
public class TempInhab {
    @Id
    @Column(name="tinhab_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long tInhabId;

    @Column(name="name")
    private String name;

    @Column(name="birth_loc")
    private String birthLoc;

    @Column(name="birth_date")
    private Date birthDate;

    @Column(name="mother_name")
    private String motherName;

    @Column(name="perma_address")
    private String permaAddress;

    @Column(name="passport_num")
    private String passportNum;

    @Column(name="student_card_num")
    private String studentCardNum;

    @Column(name="phone_number")
    private String phoneNumber;

    @Column(name="move_in_date")
    private Date moveInDate;

    @Column(name="move_out_date")
    private Date moveOutDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pk")
    private User inhab;//kollégiumi lakó

    @Column(name="comment")
    private String comment;

    public TempInhab(){

    }


    public static List<TempInhab> getTempInhabitants(){
        Session session = null;
        List tempInhabs = null;
        try {
            tempInhabs = new ArrayList<TempInhab>();
            session = HibernateConfiguration.getSession();
            Criteria criteria = session.createCriteria(TempInhab.class);
            tempInhabs = criteria.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return tempInhabs;
    }

    public static TempInhab getTInhab(long pk) {
        Session session = null;
        TempInhab tinhab = null;
        try {
            session = HibernateConfiguration.getSession();
            session.clear();
            Criteria criteria = session.createCriteria(TempInhab.class)
                    .add(Restrictions.eq("tInhabId", pk));
            tinhab = (TempInhab) criteria.uniqueResult();
            //TODO nem letezo user
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(session != null){
                session.close();
            }
        }
        return tinhab;
    }

    public void save() {
        HibernateCommon.save(this);
    }

    public long gettInhabId() {
        return tInhabId;
    }

    public void settInhabId(long tInhabId) {
        this.tInhabId = tInhabId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthLoc() {
        return birthLoc;
    }

    public void setBirthLoc(String birthLoc) {
        this.birthLoc = birthLoc;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getPermaAddress() {
        return permaAddress;
    }

    public void setPermaAddress(String permaAddress) {
        this.permaAddress = permaAddress;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getStudentCardNum() {
        return studentCardNum;
    }

    public void setStudentCardNum(String studentCardNum) {
        this.studentCardNum = studentCardNum;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getMoveInDate() {
        return moveInDate;
    }

    public void setMoveInDate(Date moveInDate) {
        this.moveInDate = moveInDate;
    }

    public Date getMoveOutDate() {
        return moveOutDate;
    }

    public void setMoveOutDate(Date moveOutDate) {
        this.moveOutDate = moveOutDate;
    }

    public User getInhab() {
        return inhab;
    }

    public void setInhab(User inhab) {
        this.inhab = inhab;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "TempInhab{" +
                "tInhabId=" + tInhabId +
                ", name='" + name + '\'' +
                ", birthLoc='" + birthLoc + '\'' +
                ", birthDate=" + birthDate +
                ", motherName='" + motherName + '\'' +
                ", permaAddress='" + permaAddress + '\'' +
                ", passportNum='" + passportNum + '\'' +
                ", studentCardNum='" + studentCardNum + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", moveInDate=" + moveInDate +
                ", moveOutDate=" + moveOutDate +
                ", inhab=" + inhab +
                ", comment='" + comment + '\'' +
                '}';
    }
}

package hu.fsmr.dorman.utils;


import hu.fsmr.dorman.configuration.HibernateConfiguration;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by blase on 2017. 01. 10..
 */
public class HibernateCommon {

    public static void save(Object o){
        try
        {
            Session session = HibernateConfiguration.getSession();
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(o);
            //session.save(o);
            transaction.commit();
            session.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public static void delete(Object o)throws Exception{
        Session session = HibernateConfiguration.getSession();
        Transaction transaction = session.beginTransaction();
        session.delete(o);
        transaction.commit();
        session.close();
    }
}
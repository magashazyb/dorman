package hu.fsmr.dorman.utils;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfChunk;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import hu.fsmr.dorman.auth.model.User;
import hu.fsmr.dorman.finance.model.Finance;
import hu.fsmr.dorman.rent.model.Inventory;
import hu.fsmr.dorman.user.model.Inhabitant;
import hu.fsmr.dorman.user.model.TempInhab;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by blase on 2017.11.19..
 */
public class PDFGenerator {
    public static final String DEST = "/home/blase/Prog/java/szakdolgozat/test.pdf";

    private List<Finance> financeInformations;
    private List<TempInhab> tempInhabList;
    private static Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font docHeaderFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
    private static Font docContentForm = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL);
    private static Font balanceHeaderFont = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);


    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        try{

            //new PDFGenerator("finance").createPdf(DEST);
            //new PDFGenerator("inhab").createFinancePdf();
            //new PDFGenerator("tInhab").createTInhabPdf(TempInhab.getTInhab(2));
            new PDFGenerator("inhab").createInhabPdf(User.getUser(1));
        }catch (Exception e){
            e.printStackTrace();
        }
        //new PDFGenerator().getPDF();
    }

    public PDFGenerator(List<Finance> finance){
        this.financeInformations = finance;
    }


    public PDFGenerator(String target){
        if( target.equals("finance")){
            financeInformations = Finance.queryAllFinanceInfo();
        }
    }

    public ByteArrayOutputStream createInhabPdf(User inhab) throws DocumentException, FileNotFoundException {
        Document document = new Document();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        //PdfWriter.getInstance(document,new FileOutputStream(DEST));
        //https://stackoverflow.com/questions/11897290/how-to-convert-itextpdf-document-to-byte-array
        String paths = "/home/blase/Prog/java/szakdolgozat/DormManager/target/DormManager-1.0-SNAPSHOT/resources/finance/";
        document.open();

        PdfPTable docHeaderTable = new PdfPTable(3);
        docHeaderTable.setWidthPercentage(100);
        docHeaderTable.addCell(getCell("",docHeaderFont, PdfPCell.ALIGN_LEFT));
        docHeaderTable.addCell(getCell("Bejelentkező adatlap",headerFont, PdfPCell.ALIGN_CENTER));
        docHeaderTable.addCell(getCell("",balanceHeaderFont, PdfPCell.ALIGN_RIGHT));
        docHeaderTable.setSpacingAfter(25f);

        PdfPTable balanceHeaderTable = new PdfPTable(3);
        balanceHeaderTable.setWidthPercentage(100);

        docHeaderTable.addCell(getCell("",docHeaderFont, PdfPCell.ALIGN_LEFT));
        docHeaderTable.addCell(getCell("Képzeletbeli kollégium beköltözési lapja", balanceHeaderFont, PdfPCell.ALIGN_CENTER));
        docHeaderTable.addCell(getCell("",balanceHeaderFont, PdfPCell.ALIGN_RIGHT));

        docHeaderTable.addCell(balanceHeaderTable);
        document.add(docHeaderTable);
        PdfPTable infoTable = new PdfPTable(2);
        infoTable.addCell(new Paragraph("Lakó neve", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getLastName()+" "+inhab.getFirstName(), docContentForm));

        infoTable.addCell(new Paragraph("Anyja neve", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getMotherName(), docContentForm));

        infoTable.addCell(new Paragraph("Állandó lakcím", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getPermaAddress(), docContentForm));

        infoTable.addCell(new Paragraph("Születés helye", docHeaderFont));
        infoTable.addCell(new Paragraph("", docContentForm));

        infoTable.addCell(new Paragraph("Születési dátum", docHeaderFont));
        infoTable.addCell(new Paragraph(null == inhab.getInhabitant().getBirthDate()
                ? ""
                : inhab.getInhabitant().getBirthDate().toString(), docContentForm));

        infoTable.addCell(new Paragraph("Lakó telefonszáma", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getPermaPhoneNumber(), docContentForm));

        infoTable.addCell(new Paragraph("Vezetékes telefonszám", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getWiredPhoneNumber(), docContentForm));

        infoTable.addCell(new Paragraph("Hozzátartozó telefonszáma", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getParentPhoneNumber(), docContentForm));

        infoTable.addCell(new Paragraph("Email", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getEmailAddress(), docContentForm));
        infoTable.addCell(new Paragraph("Szak", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getMajor(), docContentForm));
        infoTable.addCell(new Paragraph("Évfolyam", docHeaderFont));
        infoTable.addCell(new Paragraph(inhab.getInhabitant().getClassYear(), docContentForm));
        infoTable.setSpacingAfter(15f);
        document.add(infoTable);


        PdfPTable rentTable = new PdfPTable(2);
        rentTable.addCell(new Paragraph("Bérelt tárgyak", docHeaderFont));
        rentTable.addCell(new Paragraph("", docHeaderFont));
        String[] rItems = inhab.getInhabitant().getRentedItems();
        if( null != rItems){
            for (String s : rItems) {
                rentTable.addCell(new Paragraph("Tárgy neve:", docHeaderFont));
                rentTable.addCell(new Paragraph(Inventory.getInventoryItemById(Integer.valueOf(s)).getItemName(), docContentForm));
            }
        }

        rentTable.setSpacingAfter(15f);
        document.add(rentTable);

        PdfPTable dateTable = new PdfPTable(2);
        dateTable.addCell(new Paragraph("Kelt: Székesfehérvár", docHeaderFont));
        dateTable.addCell(new Paragraph(new Date().toString(), balanceHeaderFont));
        dateTable.setSpacingAfter(15f);
        document.add(dateTable);

        PdfPTable signTable = new PdfPTable(2);
        signTable.addCell(new Paragraph(".............................", docHeaderFont));
        signTable.addCell(new Paragraph(".............................", docHeaderFont));
        signTable.addCell(new Paragraph("Vendég aláírása", docHeaderFont));
        signTable.addCell(new Paragraph("Igazgató alárása", docHeaderFont));
        document.add(signTable);
        document.close();
        return byteArrayOutputStream;
    }

    public ByteArrayOutputStream createTInhabPdf(TempInhab tinhab) throws IOException, DocumentException {
        Document document = new Document();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        //PdfWriter.getInstance(document,new FileOutputStream(DEST));
        //https://stackoverflow.com/questions/11897290/how-to-convert-itextpdf-document-to-byte-array
        String path = "/home/blase/Prog/java/szakdolgozat/DormManager/target/DormManager-1.0-SNAPSHOT/resources/finance/";
        document.open();

        PdfPTable docHeaderTable = new PdfPTable(3);
        docHeaderTable.setWidthPercentage(100);
        docHeaderTable.addCell(getCell("",docHeaderFont, PdfPCell.ALIGN_LEFT));
        docHeaderTable.addCell(getCell("Bejelentkező adatlap",headerFont, PdfPCell.ALIGN_CENTER));
        docHeaderTable.addCell(getCell("",balanceHeaderFont, PdfPCell.ALIGN_RIGHT));
        docHeaderTable.setSpacingAfter(25f);

        PdfPTable balanceHeaderTable = new PdfPTable(3);
        balanceHeaderTable.setWidthPercentage(100);

        docHeaderTable.addCell(getCell("",docHeaderFont, PdfPCell.ALIGN_LEFT));
        docHeaderTable.addCell(getCell("Eseti szállás igénybevételéhez", balanceHeaderFont, PdfPCell.ALIGN_CENTER));
        docHeaderTable.addCell(getCell("",balanceHeaderFont, PdfPCell.ALIGN_RIGHT));

        docHeaderTable.addCell(balanceHeaderTable);
        document.add(docHeaderTable);
        PdfPTable infoTable = new PdfPTable(2);
        infoTable.addCell(new Paragraph("Név", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getName(), docContentForm));

        infoTable.addCell(new Paragraph("Születési helye, ideje", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getBirthLoc()+", "+tinhab.getBirthDate(), docContentForm));

        infoTable.addCell(new Paragraph("Anyja neve", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getMotherName(), docContentForm));

        infoTable.addCell(new Paragraph("Állandó akhely", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getPermaAddress(), docContentForm));

        infoTable.addCell(new Paragraph("Személyi vagy útlevél igazolvány száma", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getPassportNum(), docContentForm));

        infoTable.addCell(new Paragraph("Diákigazolvány szám", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getStudentCardNum(), docContentForm));

        infoTable.addCell(new Paragraph("Telefonszám", docHeaderFont));
        infoTable.addCell(new Paragraph(tinhab.getPhoneNumber(), docContentForm));

        infoTable.addCell(new Paragraph("Érkezés napja", docHeaderFont));
        infoTable.addCell(new Paragraph(null == tinhab.getMoveInDate()
                ? ""
                : tinhab.getMoveInDate().toString(), docContentForm));

        infoTable.addCell(new Paragraph("Távozás napja", docHeaderFont));
        infoTable.addCell(new Paragraph(null == tinhab.getMoveOutDate()
                ? ""
                : tinhab.getMoveOutDate().toString(), docContentForm));

        infoTable.addCell(new Paragraph("Vendégéjszakák száma", docHeaderFont));
        infoTable.addCell(new Paragraph("KISZAMOLNI AZ ERKEZES TAVOZAS KULONBSEGET", docContentForm));

        infoTable.addCell(new Paragraph("Szoba száma", docHeaderFont));
        infoTable.addCell(new Paragraph("....", docContentForm));


        infoTable.addCell(new Paragraph("Szállásdíj összege", docHeaderFont));
        infoTable.addCell(new Paragraph(".......,-Ft/fő/éj  Összesen: .................Ft", docContentForm));

        infoTable.setSpacingAfter(15f);
        document.add(infoTable);

        PdfPTable dateTable = new PdfPTable(2);
        dateTable.addCell(new Paragraph("Kelt: Székesfehérvár", docHeaderFont));
        dateTable.addCell(new Paragraph(new Date().toString(), balanceHeaderFont));

        document.add(dateTable);

        PdfPTable signTable = new PdfPTable(2);
        signTable.addCell(new Paragraph(".............................", docHeaderFont));
        signTable.addCell(new Paragraph(".............................", docHeaderFont));
        signTable.addCell(new Paragraph("Vendég aláírása", docHeaderFont));
        signTable.addCell(new Paragraph("Igazgató alárása", docHeaderFont));
        document.add(signTable);
        document.close();
        return byteArrayOutputStream;
    }


    public ByteArrayOutputStream createFinancePdf() throws IOException, DocumentException {
        Document document = new Document();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, byteArrayOutputStream);
        ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
        String path = extContext.getRealPath("//resources//finance//");
        document.open();

        PdfPTable docHeaderTable = new PdfPTable(3);
        docHeaderTable.setWidthPercentage(100);
        docHeaderTable.addCell(getCell("",docHeaderFont, PdfPCell.ALIGN_LEFT));
        docHeaderTable.addCell(getCell("Pénzügyi kimutatás",headerFont, PdfPCell.ALIGN_CENTER));
        docHeaderTable.addCell(getCell("",balanceHeaderFont, PdfPCell.ALIGN_RIGHT));
        docHeaderTable.setSpacingAfter(25f);

        PdfPTable balanceHeaderTable = new PdfPTable(3);
        balanceHeaderTable.setWidthPercentage(100);

        docHeaderTable.addCell(getCell("",docHeaderFont, PdfPCell.ALIGN_LEFT));
        docHeaderTable.addCell(getCell("Egyenleg: "+getBalance()+" Ft", balanceHeaderFont, PdfPCell.ALIGN_CENTER));
        docHeaderTable.addCell(getCell("",balanceHeaderFont, PdfPCell.ALIGN_RIGHT));

        docHeaderTable.addCell(balanceHeaderTable);
        document.add(docHeaderTable);

        for (Finance financeInformation : financeInformations) {
            PdfPTable infoTable = new PdfPTable(3);
            List<String> financeImages = financeInformation.getImages();
            infoTable.addCell(new Paragraph("Cím", docHeaderFont));
            infoTable.addCell(new Paragraph("Pénzmozgás", docHeaderFont));
            infoTable.addCell(new Paragraph("Megjegyzés", docContentForm));
            infoTable.addCell(financeInformation.getTitle());
            infoTable.addCell(financeInformation.getMoneyMovement().toString() + "ft");
            infoTable.addCell(financeInformation.getComment());
            for (String financeImage : financeImages) {
                String imgPath = path+financeImage;
                Image img11 = Image.getInstance(imgPath);
                infoTable.addCell(img11);
            }
            infoTable.setSpacingAfter(15f);
            document.add(infoTable);
        }
        document.close();
        return byteArrayOutputStream;
    }

    private int getBalance(){
        int balance = 0;
        for (Finance finance : financeInformations) {
            balance += finance.getMoneyMovement();
        }
        return balance;
    }

    private PdfPCell getCell(String text, Font font, int alignment) {
        PdfPCell cell = new PdfPCell(new Paragraph(text, font));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }
}
